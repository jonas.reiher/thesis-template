# LaTeX Thesis Template

## Disclaimer

This is my personal template and may therefore be far from perfect for public use. If you have any questions, feel free to reach out!

## Compiling

This document needs to be compiled using LuaLaTeX. The output file is then written to `main.tex`.
There are multiple options to do this:

### Makefile

This repository contains a `makefile`.
Just run `make` in your command line to start the build.

Note: Use `make clean` to remove auxiliary files from your workspace.

### Manual

In your command line, run the following:
```
lualatex main
biber main
lualatex main
lualatex main
```
This is what the makefile does behind the scenes.

### Using Visual Studio Code

*This is my preferred way to work with LaTeX documents in general.*

Install the [LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) Extension and trigger `Build LaTeX project` from the TeX-menu in your sidebar.
This recognizes the TeX magic commands at the beginning of `main.tex`.

Note: Use `make clean` to remove auxiliary files from your workspace.

### Using Overleaf

When using [Overleaf](https://www.overleaf.com), make sure to specify the following settings:
- Compiler: `LuaLaTeX`
- Main Document: `main.tex`

## Hints

### General

Make sure to use [Git LFS](https://git-lfs.github.com) for images and PDFs.

Use the commands in `main.tex` to populate the title page.

Specify acronyms or abbreviations (rendered differently) in `resource/acronyms.tex`.

Add your references in bibtex format to `resource/references.bib`.

Most or all functionalities should be demonstrated in the placeholder chapters and sections of this template.

When adding new chapters, create a `.tex`-file in the `chapters` folder and include it in `main.tex`.

Templates for image arrangements and tables can be found in `templates`.

Use `\TODO{your note}` to create remarks on the page border.

When finished, add `final` to the options of `\documentclass` in the first line of `header.tex`. This disables the TODO-notes package.

Also, when finished, use `\formatdate{dd}{mm}{yyyy}` instead of `\today` for the date in `main.tex`.

If you have the fonts Cambria and Cambria Math installed in your system, you can remove the `fonts` folder and simply replace the `\setmainfont` and `\setmathfont` commands in `header.tex` with the following.
```
\setmainfont{Cambria}
\setmathfont{Cambria Math}
```

... more to come.

### Logo Usage

Replace the general RWTH logo by the specific logo for your institute in `graphics/titlepage/rwth-logo.pdf` and/or in `pages/titlepage.tex` in line 6.

When using an RWTH logo, make sure the surrounding safe space is included in your file.

For legal RWTH logo use on your thesis refer to [this page](https://www.rwth-aachen.de/cms/root/Studium/Im-Studium/Pruefungen-Abschlussarbeiten/~hjxv/Hinweise-zu-schriftlichen-Arbeiten/?lidx=1) with usage requirements.