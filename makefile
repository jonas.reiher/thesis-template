# root filename without .tex
file = main

.PHONY: build pdf clean

build:
	lualatex ${file}
	biber ${file}
	lualatex ${file}
	lualatex ${file}

pdf: build clean

clean:
	find . -type f -name "*.aux" -delete
	rm -f ${file}.bbl ${file}.bcf ${file}.blg ${file}.lof ${file}.log ${file}.lot ${file}.tdo ${file}.toc ${file}.run.xml ${file}.synctex.gz *.synctex\(busy\)